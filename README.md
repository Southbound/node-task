# Node Task

Welcome to Zombocom. You can do anything at Zombocom.

---
- [Node Task](#node-task)
    - [Installation steps](#installation-steps)
    - [Checklist](#checklist)
    - [Ööh](#ööh)

---

## Installation steps

1. Clone repository `git clone`
2. Koitas nyt
3. Mene roskiin


Näitä ohjeita seuraamalla sinulla *pitäisi* olla seuraava pätkä koodia:

```
const express = require('express');
const app = express();

app.get('', (req, res) => {
  res.send('ok');
});

app.listen(3000, () => {
  console.log('Server listening on localhost:3000');
});
```

Jos ei onnistunut, katsele vaikka tätä tekemääni Minecraft-skiniä:


![Indiana Jones in Minecraft](./Indyselfie.jpg)


---

## Checklist

- [x] Merkkaa tämä tehtävä suoritetuksi
- [ ] Tätä älä suorita
- [ ] Älä tätäkään
- [ ] ***Ei.***

---
## Ööh

| Peruna | Lisuke     |
| ------ | ---------- |
| Muusi  | Nakit      |
| Lohko  | Lihapullat |