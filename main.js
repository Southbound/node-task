const express = require('express');
const app = express();
const PORT = 3000;

/**
 * Add two numbers together
 * @param {Number} a 1st param
 * @param {Number} b 2nd param
 * @returns {Number} sum of a and b
 */
const add = (a, b) => {
    return a + b;
}

app.get('', (req, res) => {
    res.send('Morienttes');
})

app.get('/add', (req, res) => {
    console.log(req.query);
    const sum = add(req.query.a, req.query.b);
    res.send(sum);
})

app.listen(PORT, () => console.log(
    `Server listening on http://localhost:${PORT}`
));